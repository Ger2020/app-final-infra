FROM ubuntu:latest
LABEL version="1"
LABEL description="app-final-infra.git"

ARG DEBIAN_FRONTEND=noninteractive

#Se instala el servicio del servidor web Apache
RUN apt-get update && apt-get upgrade -y
RUN apt-get install apache2 -y

#Se obtiene el fichero del respositorio Git Lab
ADD https://gitlab.com/Ger2020/app-final-infra/-/raw/main/index.html /var/www/html
RUN chmod +r /var/www/* -R

#Se obtiene el archivo default del repositorio Git Lab
ADD https://gitlab.com/Ger2020/app-final-infra/-/raw/main/default.conf /etc/apache2/sites-available

#Exponemos los puertos que usara la aplicacion
EXPOSE 80 443



